# Syscall structure of DaeDream


## Architecture-independent overview of syscalls in DaeDream

DaeDream will always have a single syscall vector in all architectures. Syscalls will be dispatched in the following manner:

- Arguments to the syscall vector will be done only using registers
- The registers' argument order is in accordance to the register argument order of their System V ABI Calling Convention. For example, in the x86-64 architecture, the first argument would be in the rdi register.
- If an architecture doesn't happen to have any well-defined calling convention or doesn't have a well-defined register argument, it will be done in the order of the general-purpose registers. For example: If this OS were to theoretically be created for the 6502, the first argument would be in the A register and the second would be in the X register.

Each syscall will have two arguments in this order:

1. Syscall number
2. Syscall Argument Structure Pointer (SASP)

### General Syscall Argument Structure(SAS) Specifications

Each syscall will have their own unique SAS which must be filled out. Each SAS will be of a fixed size as well, so no need to worry about variable sized structures.

Each SAS will have one of two sizes:
16-bit signed/unsigned integers
pointer-sized unsigned integers

Each SAS must always follow alignment requirements of the architecure.


## Architecture specific specifications

The interrupt you want to invoke for the syscall is 255.

### x86-64

For the x86-64, all arguments are 64-bit integers. Syscall numbers are put in rdi and the SASP goes in rsi.
