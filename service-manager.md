# DaeDream Service Manager

## Abstract

There needs to be a way to keep regular ol' user-mode processess from gaining access to the actual underlying device abstractions such as DMA, etc, as this could be a serious problem, and although I'd love to just say "Well, if it's running under root, then it should have access to thsese things, I really don't like this idea. Instead, ONE process will be given the ability to create what's known as "Service-level processes" that will have access to basic device driver abstractions.
