# `daedream`'s Memory Map

## Discussion about the way the memory map is talked about

**IMPORTANT PLEASE READ SO YOU CAN UNDERSTAND THE MEMORY MAP**

Daedream's memory map will be talked about in terms of "The topmost level of paging".
For the example of what this means, we'll use the x86_64 architecture.
x86_64's architecure lists its pages as 4 level paging (or 5 depending on if you want to support that or not).
This top level will divide memory into 512GiB pages. All memory mapping will be described as this "Top level pages".
This makes it much easier to implement (Although this is definitely subject to change).
Since these are variable depending on the architecture, it's not wise to say "This is always this large" etc, because it's not.

For the sake of this document, we'll be discussing the top-most part of the table as `T` and the bottom of it as `B`. The page size will be listed as `P`

## Virtual Memory

### Kernel space

Kernel space will be at `T`. This area is dedicated to statically known kernel stuff, and will always be mapped, even in userspace (Although mapped inaccessable).

### Kernel heap

This will only be mapped in kernel mode. This will be at `T - 1`. This will hold any sort of heap allocations done by the kernel, meaning the kernel heap will only have a size of `P` at max.

### Identity mapping

The bootloader will give the kernel a space where physical memory is mapped. This space will only be mapped in kernel space. This space is undetermined.

### Program placement

#### Stack

Programs will have their stacks placed at `T - 1` and will grow downwards.

#### Load Address

Programs will be loaded wherever their load address requests them to be loaded, BUT they cannot be loaded in `T - 1` or `T` as these spaces are reserved.

## Physical Memory

### Allocation growth

All general-purpose allocation will be done from the top of physical memory space downwards.

### DMA Space

DMA space will be the lowest point we can find in physical memory, and will grow upwards. This is to keep compatibility with various MMIO spaces and allocation

### Memory-Mapped IO

MMIO will be just wherever this is. This will be reserved-space and will NOT be used by the operating system.
