# The boot process of DaeDream: V3

## Loading the kernel:

### Stage 1: [limine](https://github.com/limine-bootloader/limine), or whatever bootloader

I'll speak of this as if it's limine.
Limine will set up the system in a manner where the kernel is loaded in high memory, the system is set up, and various system infos are given to the kernel.
In reality, the system needs to be able to run code.

### Stage 2: Entry Point

The entry point will finish setting up so that the kernel can be run.
The machine should be in this state:
- Kernel is loaded into high memory.
- Modules are passed to the kernel
- Various system infos are passed to the kernel
- The kernel is given a way to get from virtual memory to physical memory (to traverse the page tables).
- Some sort of debugging console is given to the kernel.

All of this is taken care of in the `bootloader` package in the kernel tree.


### Stage 3: Kernel main

kmain is run, and the kernel does its normal stuff (setting up pmm, vmm, etc.)
